## Project: "Open International Project "jiskefet". API development for making Tech work-shift reports with a template"

Second version of the project which is using express for the server, and ejs + html-pdf for generating pdf from an html template.

## Participants
| Study group | Username | Fullname |
|----------------|------------------|--------------------------| 
| 181-351 | @Shadthrough | Bogachev M. G. |
| 181-351 | @TheLast38 | Semennikov K.V. |
| 181-351 | @Anton250 | Petrov A.D. |

## Participants' personal contribution

### Bogachev M. G.

- Developed site design;
- Made PDF-report templates;

### Semennikov K.V.

- Wrote a part of the server using Express and set html-pdf;
- Made an API-Request;

### Petrov A.D.

- Wrote a part of the server using Express and set html-pdf;
- Rewrote code for generating pdf from html files using the ejs and html-pdf libraries;

## Проект: «Международный открытый проект jiskefet. Разработка API для создания отчётов технических смен по шаблону»

Вторая версия использует библиотеку express для сервера, и библиотеки ejs и html-pdf для генерации pdf из html шаблона.

## Участники

| Учебная группа | Имя пользователя | ФИО                      |
|----------------|------------------|--------------------------|
| 181-351       | @Shadthrough       | Богачев М.Г.              |
| 181-351        | @TheLast38       | Семенников К.В.              |
| 181-351        | @Anton250      | Петров А.Д. |

## Личный вклад участников

### Богачев М.Г.

- Разработал дизайн сайта;
- Cоздал шаблоны для pdf-отчётов;

### Семенников К.В.

- Написал часть сервера с использованием express и настроил html-pdf;
- Сделал API запрос;

### Петров А.Д.

- Написал часть сервера с использованием express и настроил html-pdf;
- Переписал код для генерации pdf из html файлов с использованием библиотек ejs и html-pdf;

# Jiskefet_pdf
1. Installing nodejs. If you have already installed nodejs on your machine, skip this.
+ ```$ sudo apt update ```
+ ```$ sudo apt install build-essential checkinstall ```
+ ```$ sudo apt install libssl-dev```
+ ```$ wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash```
+ restart the terminal
+ check available versions via ```$ nvm ls-remote```
+ install needed version for example 10.15.2 via ```$ nvm install 10.15.2```
+ use the installed version via ```$ nvm use 10.15.2```
+ to set default nodejs version write ```$ nvm alias default 10.15.2```
+ also you can check installed versions of nodejs via ```$ nvm list```
+ Useful notes:
  + you can delete some versions: first of all you need to turn off version via ```$ nvm deactivate 10.15.2```
  + then ```$ nvm uninstall 10.15.2```
2. clonning jiskefetPDF
+ ```$ git clone https://gitlab.com/Thelast38/jiskefet_pdf```
3. deploying server
+ ```$ cd jiskefet_pdf```
+ ```$ npm install```
4. running server
+ ```$ node app```
+ the server is running on 3010 port
+ when you have closed the terminal server will shut down. To forbid him shutting down after ```$ node app``` you need to write ```$ disown -h %1``` and then ```$ bg 1```, now the srever will not shut down when ypu close the terminal
+ to stop the server you need to write ```$ killall node```
5. Everytime when you restart the terminal, you need to write ```$ nvm use 10.15.2``` or your version nodejs

