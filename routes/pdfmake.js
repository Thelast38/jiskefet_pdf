const express = require('express');
const router = express.Router();
const app = express();
const fs = require('fs');
const ejs = require('ejs');
// Возвращает случайное целое число между min (включительно) и max (не включая max)
// Использование метода Math.round() даст вам неравномерное распределение!
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
const pdfMake = require('html-pdf');

var date = new Date();
var day = date.getDate();
if (day < 10){
  day = "0" + day;
}
var month = date.getMonth() + 1;
if (month < 10){
  month = "0" + month;
}
const year = date.getFullYear();
var hour = date.getHours();
if (hour < 10){
  hour = "0" + hour;
}
var min = date.getMinutes();
if (min < 10){
  min = "0" + min;
}
var allDate = String(hour) + ":" + String(min) + " " + String(day) + "." + String(month) + "." + String(year);


router.get('/:apiname', function(req, res){
  var temper=[];
  temper = req.query;
console.log(temper);
var data = temper.data;
var temp = temper.temp;
const params = {data: data ,temp: temp};
  ejs.renderFile(__dirname + '/get1_template.ejs', params, function(err, html) {
    const options = { format: 'A4', orientation: 'portrait'};
      const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
        if (err){
          res.end("Error");
          console.log(err);
        }
        console.log(allDate);

        res.writeHead(200, {
          "Content-type": "application/pdf",
          "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", allDate)
        });
        res.end(buffer);
      });
    });
});

router.post('/pdf', function(req, res, next){

  const fname = req.body.fname;
  const lname = req.body.lname;
  const occupation = req.body.occupation;
  const bugReport = req.body.bugReport;
  var temp=[];
  for (var i = 0; i < 14; i++) {
    temp.push(getRandomInt(100,300));
  }
  const params = {firstname : fname, lastname: lname, occupation: occupation, date: allDate, bugReport: bugReport,temp: temp};
  ejs.renderFile(__dirname + '/template.ejs', params, function(err, html) {
    const options = { format: 'A4', orientation: 'portrait'};
      const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
        if (err){
          res.end("Error");
          console.log(err);
        }
        console.log(allDate);

        res.writeHead(200, {
          "Content-type": "application/pdf",
          "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", allDate)
        });
        res.end(buffer);
});
});
});



module.exports = router;
